/*
  每一帧只发送一个端口的数据
*/

int portCount  = 34; //接口数量
int allPort[34] = {2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35};
String allPortStr[34];
int beforData[34] ;

// int portCount  = 2; //接口数量
// int allPort[2] = {6,7};
// String allPortStr[2];
// int beforData[2];

char result[25]; 
String boardId = "1";  //板子id,一号板子
  String separator = "-";   //分隔符
void setup() {
  
  Serial.begin(9600);
  for(int i=0;i<portCount;i++)
  {
    pinMode(allPort[i],INPUT);
    char str[25];
    itoa(allPort[i],str,10);
    allPortStr[i] = str;   
    beforData[i] = -1;
  }
}
 int num = 0;
void loop() {
  int port = allPort[num]; //板子
  int data = digitalRead(port);
  if(data != beforData[num])
  {
      String bi = boardId;  
      String resultCon = bi;    
      resultCon.concat(separator);//第1次连接 "1-"
      resultCon.concat(allPortStr[num]);//第2次连接 "1-4"
      resultCon.concat(separator);//第3次连接 "1-4-"

      itoa(data,result,10);
      resultCon.concat(result);//第3次连接 "1-4-0"
      Serial.println(resultCon); 
      beforData[num] = data;
  }  
  num = num + 1;
  if(num >= portCount)
  {
       delay(10);
      num = 0;         
  } 

}