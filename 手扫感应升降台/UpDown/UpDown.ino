int IN1 = 6;
int IN2 = 7;

int MOVE_PIN = 12;
// 针脚读取到的上个值（有效值）
int last_value = 0;
// 上次有效值更替时间
unsigned long last_millis = 0;
const int LIMIT_MILLIS = 3000;

void setup() {
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);

  // 初始化升起
  digitalWrite(IN1,1);
  digitalWrite(IN2,0);

  Serial.begin(9600);
  pinMode(MOVE_PIN, INPUT);
  digitalWrite(MOVE_PIN, last_value);
  last_millis = millis();
}

void loop() {
  int new_value = digitalRead(MOVE_PIN);
  unsigned long new_millis = millis();
  if(new_value==last_value){
    return;
  }
  
  last_value = new_value;

  if((new_millis-last_millis)<=LIMIT_MILLIS){
    return;
  }

  // 升降（[IN1,IN2] [1,0]升 [0,1]降）
  digitalWrite(IN1, !digitalRead(IN1));
  digitalWrite(IN2, !digitalRead(IN2));
  last_millis = new_millis;
  delay(30);
}
